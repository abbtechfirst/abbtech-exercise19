package org.example.abbtechmodulethreeexercise19.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;
@Entity
@Table(name = "Book")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BookID")
    private Long bookId;

    @Column(name = "Title")
    private String title;

    @Column(name = "Author")
    private String author;

    @OneToMany(mappedBy = "book")
    private List<Professor> professors;
}
