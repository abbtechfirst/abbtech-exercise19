package org.example.abbtechmodulethreeexercise19.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Entity
@Table(name = "StudentIDCard")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class StudentIDCard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CardID")
    private Long cardId;

    @Column(name = "CardNumber")
    private String cardNumber;

    @Column(name = "ExpiryDate")
    private Date expiryDate;

    @OneToOne
    @JoinColumn(name="CardID")
    private Student student;
}
