package org.example.abbtechmodulethreeexercise19.model;


import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;

class StudentClassId implements Serializable {
    private Long studentId;
    private Long classId;
}
@Entity
@Table(name = "StudentClass")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class StudentClass {
    @EmbeddedId
    private StudentClassId id;

    @ManyToOne
    private Student student;

    @ManyToOne
    private Course classId;
}