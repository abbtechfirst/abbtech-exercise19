package org.example.abbtechmodulethreeexercise19.model;

import jakarta.persistence.*;
import lombok.*;
import java.util.List;

@Entity
@Table(name = "Department")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DepartmentID")
    private Long departmentId;

    @Column(name = "DepartmentName")
    private String departmentName;

    @OneToMany(mappedBy = "department")
    private List<Professor> professors;
}
