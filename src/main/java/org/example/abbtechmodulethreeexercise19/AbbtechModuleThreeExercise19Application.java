package org.example.abbtechmodulethreeexercise19;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class AbbtechModuleThreeExercise19Application {

    public static void main(String[] args) {
        SpringApplication.run(AbbtechModuleThreeExercise19Application.class, args);
    }

}
