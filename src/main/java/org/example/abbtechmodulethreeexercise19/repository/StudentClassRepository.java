package org.example.abbtechmodulethreeexercise19.repository;

import org.example.abbtechmodulethreeexercise19.model.Professor;
import org.example.abbtechmodulethreeexercise19.model.Student;
import org.example.abbtechmodulethreeexercise19.model.StudentClass;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentClassRepository extends JpaRepository<StudentClass, Long> {
}
