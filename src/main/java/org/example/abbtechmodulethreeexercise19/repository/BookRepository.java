package org.example.abbtechmodulethreeexercise19.repository;

import org.example.abbtechmodulethreeexercise19.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {
}
