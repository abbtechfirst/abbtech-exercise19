package org.example.abbtechmodulethreeexercise19.repository;

import org.example.abbtechmodulethreeexercise19.model.*;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
}

