package org.example.abbtechmodulethreeexercise19.repository;

import org.example.abbtechmodulethreeexercise19.model.Department;
import org.example.abbtechmodulethreeexercise19.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}

