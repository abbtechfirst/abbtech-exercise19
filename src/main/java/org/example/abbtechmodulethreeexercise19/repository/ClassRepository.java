package org.example.abbtechmodulethreeexercise19.repository;

import org.example.abbtechmodulethreeexercise19.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassRepository extends JpaRepository<Course, Long> {
}
